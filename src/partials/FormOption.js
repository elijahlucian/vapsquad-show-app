import React from 'react'

export default function FormOption({text}) {
  return <option>{text}</option>
}

