import React from 'react';
import { Alert, 
  Button, 
  Col, 
  Container, 
  Form, 
  Row, 
  ToggleButtonGroup, 
  ToggleButton 
}  from 'react-bootstrap';
import FormOption from './partials/FormOption'
import './App.css';
import axios from 'axios'

const req = axios.create({
  baseURL: (process.env.NODE_ENV === 'development') ? 'https://admin.vapsquad.com/api' : '/api',
})

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      error: null,
      segment_name: '',
      segment_details: '',
      scriptName: '',
      actor: '',
      actorsInBooth: [],
      actors: [],
      segments: [],

      // randomized data from server
      to_randomize: [
        'accents',
        'adjectives',
        'animals',
        'countries',
        'emotions',
        'nouns',
        'verbs',
        'voices',
        'voiceStyles'
      ],
    }
  }

  updateActor = (e) => {
    this.setState({actor: e.target.value})
  }

  publishCast = (e) => {
  }

  publishSegment = (e) => {
    let segment = {
      name: this.state.segment_name,
      details: this.state.segment_details,
    }
    req.post('/segment', segment)
    e.preventDefault()
  }

  updateSegmentDetails = (e) => {
    this.setState({segment_details: e.target.value})
  }
  updateSegmentName = (e) => {
    this.setState({segment_name: e.target.value})
  }
  clearSegmentDetails = (e) => {
    this.setState({segment_details: ''})
    e.preventDefault()
  }

  addToCast = (e) => {
    console.log("TODO: Modal Box")
    e.preventDefault()
  }
  removeFromCast = (e) => {
    let actorIndex = e.target.value
    let cast = this.state.cast
    cast.splice(actorIndex,actorIndex + 1)
    this.setState({cast})
  }
  updateBooth = (value, e) => {
    req.post('in_booth', value.map(id => this.state.actors[id - 1]))
    this.setState({ inBooth: value })
  }
  randomize = (e) => {
    let thing = e.target.value
    req.get(`random/${thing}`).then(res => {
      this.setState({[thing]: res.data})
    })
  }

  componentDidMount = () => {
    req.get('data/segments').then(response => {
      this.setState({segments: response.data})
    })
    req.get('data/actors').then(response => {
      this.setState({actors: response.data})
    })
    req.get('indexes/random').then(response => {
      console.log("RANDO =>",response.data)
      // eventually fix to filter actors and segments
      // this.setState({to_randomize: response.data})
    })
  }

  componentDidUpdate() {
    
  }

  render() {
    let actorList =this.state.actors.map((actor, i) => {
      return <ToggleButton className="actorButtons" key={i} value={actor.id}>{actor.handle}</ToggleButton>
    })

    return (
      <Container className="App">
        <Row className="title">
          <Col>
            <h1>Vapsquad Control Panel</h1>
            {this.state.error && <Alert dismissible variant="warning" onClick={()=>{this.setState({error: null})}}>{this.state.error}</Alert>}
          </Col>
        </Row>

        <Row className="booth-pick">
            <h3 className="actorButtons-header">Who's In The Booth?</h3>
            <ToggleButtonGroup
              className="actorButtonGroup"
              value={this.state.inBooth}
              type="checkbox"
              onChange={this.updateBooth}
            >
              {actorList}
            </ToggleButtonGroup>
        </Row>
        <Row>
          <h1 className="title">The Randomizer</h1>
        </Row>
        <Row className="randomizer">
          {this.state.to_randomize.map((item, i) => {
            return (
              <Col key={i}>
                <Button size="lg" value={item} onClick={this.randomize}>{item}</Button>
                <h3>{this.state[item]}</h3>
              </Col>
            )
          })}
        </Row>

        <Row className="characters">
          <Col>
          <h1>Segment</h1>
          <p>Pick show segment and enter details</p>
          <Form onSubmit={this.publishSegment}>
            <Form.Label>Show Segment</Form.Label>
            <Form.Control defaultValue="Select Segment" as="select" onChange={this.updateSegmentName}>
              {this.state.segments.map((item,i) => {
                return <FormOption key={i} value={item.id} text={item.name}/>
              })}
            </Form.Control>
            <Form.Label>Details (script name, game name, subreddit)</Form.Label>
            <Form.Control type="text" value={this.state.segment_details} onChange={this.updateSegmentDetails} placeholder="Enter Script" />
            <Button variant="primary" type="submit">Update Show Segment</Button>
            <Button variant="secondary" onClick={this.clearSegmentDetails}>Clear Show Details</Button>
          </Form>
          </Col>
        </Row>

        <Row className="todo">
          <Col>
          <h1>App Todo</h1>
          <ul>
            <li>Add New Actor Modal Box</li>
            <li>Add Wrapping to actor buttons</li>
            <li>Add Dropdown selector to populate daily actors</li>
            <li>sort actors by times in booth.</li>
          </ul>
          
          <h1>Server Todo</h1>
          <ul>
            <li>Add sqlite database</li>
            <li>add default gets for initial overlay state</li>
            <li>add default gets for frontend app population</li>
            <li>add get requests for current state</li>
            <li>get server up and running on pi</li>
          </ul>

          <h1>Overlay Todo</h1>
          <ul>
            <li>add shader pass</li>
            <li>add Dank Planet</li>
            <li>add SFX</li>
            <li>abstract out the font builders and shit</li>
            <li>find way to load font to window for quicker loading.</li>
            <li>segment chane animation will use last segment's width and height and scale down from that size</li>
            <li>figure out colors for overlay</li>
            <li>chroma color to match booth</li>
            <h2>done</h2>
            <li>animate names on entry - with different lengths - possibly spin ease in...</li>
          </ul>
          <h1>Telegram Bot</h1>
          <ul>
            <li>let users add randomizers</li>
            <li>update user info</li>
          </ul>    
          <h1>Telegram Bot</h1>

          </Col>
        </Row>
      </Container>
    );
  }
  
}

export default App;
